# Hi, my name is Mar ⛰️

- Working as a System Engineer in Infrastructure Automation [@SVA](https://www.sva.de)
- Learning, always learning
- [CALMS](https://www.atlassian.com/devops/frameworks/calms-framework)-Mindset

## Hobbies (currently)

- [Pen and Paper](https://ulisses-us.com/games/tde/) (Dungeon Master 🙆🏻)
- [Birding](https://www.audubon.org/birding/how-to-start-birding) (started and it's fun!)
- Watching [Arte](https://www.arte.tv) (there is always a perl to find!)

## Quotes

*I have no special talent. I am only passionately curious.* - A. Einstein (1952)

*Nihili est vir qui mundum non reddat meliorem.* (R. Scott's "Kingdom of Heaven")

*It is the small everyday deeds of ordinary folk that keep the darkness at bay. Small acts of kindness and love.* - Gandalf (P. Jackson's "The Hobbit")